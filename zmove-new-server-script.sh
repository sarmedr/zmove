#!/bin/bash
echo Restoring the domains

for i in `cat /backups/zmove/domains.txt `; do  zmprov cd $i zimbraAuthMech zimbra ;echo $i ;done

#restore accounts
echo -e "\nRestoring accounts"
USERPASS="/backups/zmove/userpass"
USERDDATA="/backups/zmove/userdata"
USERS="/backups/zmove/emails.txt"
for i in `cat $USERS`
do
givenName=$(grep givenName: $USERDDATA/$i.txt | cut -d ":" -f2)
displayName=$(grep displayName: $USERDDATA/$i.txt | cut -d ":" -f2)
shadowpass=$(cat $USERPASS/$i.shadow)
tmpPass="CHANGEme"
zmprov ca $i CHANGEme cn "$givenName" displayName "$displayName" givenName "$givenName"
zmprov ma $i userPassword "$shadowpass"
done

#restoring mailbox
echo -e "\nRestoring mailboxes...\nThis may take a while"
for i in `cat /backups/zmove/emails.txt`; do zmmailbox -z -m $i postRestURL "/?fmt=tgz&resolve=skip" /backups/zmove/mailbox/$i.tgz ;  echo "$i -- finished "; done

#restoring distribution list
echo -e "\nCreating distribution lists"
for i in `cat /backups/zmove/dlist.txt`; do zmprov cdl $i ; echo "$i -- done " ; done

echo -e "\nRestoring distribution lists"
for i in `cat /backups/zmove/dlist.txt`
do
	for j in `grep -v '#' /backups/zmove/dlist_members/$i.txt |grep '@'`
	do
	zmprov adlm $i $j
	echo " $j member has been added to list $i"
	done

done

# restoring aliases #
echo -e "Restoring aliases"
for i in `cat /backups/zmove/emails.txt`
do
	if [ -f "/backups/zmove/alias/$i.txt" ]; then
	for j in `grep '@' /backups/zmove/alias/$i.txt`
	do
	zmprov aaa $i $j
	echo "$i HAS ALIAS $j --- Restored"
	done
	fi
done
