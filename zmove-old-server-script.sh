#!/bin/bash

## create backup directory
mkdir -p /backups/zmove
chown -R zimbra:zimbra /backups/zmove

echo Backing up domains
su - zimbra -c 'zmprov gad > /backups/zmove/domains.txt'
cat /backups/zmove/domains.txt


echo -e "\nBacking up admin users"
su - zimbra -c 'zmprov gaaa > /backups/zmove/admins.txt'
cat /backups/zmove/admins.txt


echo -e "\nBacking up email addresses"
su - zimbra -c 'zmprov -l gaa > /backups/zmove/emails.txt'

for email in `cat /backups/zmove/emails.txt`;
do
if [[ $email != 'admin'* ]] && [[ $email != 'spam'* ]] && [[ $email != 'ham.'* ]] && [[ $email != 'virus-quarantine.'* ]] && [[ $email != 'galsync.'* ]]
	then echo $email >> /backups/zmove/tmp
fi;
done;
cat /backups/zmove/tmp > /backups/zmove/emails.txt
rm -rf /backups/zmove/tmp
cat /backups/zmove/emails.txt

echo -e "\nBacking up user passwords"
su - zimbra -c "mkdir /backups/zmove/userpass"
su - zimbra -c 'for i in `cat /backups/zmove/emails.txt`; do zmprov  -l ga $i userPassword | grep userPassword: | cut -d" " -f2 > /backups/zmove/userpass/$i.shadow; done'

echo -e "\nBacking up userdata"
su - zimbra -c "mkdir /backups/zmove/userdata"
su - zimbra -c 'for i in `cat /backups/zmove/emails.txt`; do zmprov ga $i  | grep -i Name: > /backups/zmove/userdata/$i.txt ; done'

echo -e "\nBacking up email accounts"
su - zimbra -c "mkdir /backups/zmove/mailbox"
su - zimbra -c 'for email in `cat /backups/zmove/emails.txt`; do  zmmailbox -z -m $email getRestURL "/?fmt=tgz" > /backups/zmove/mailbox/$email.tgz ;  echo $email ; done'


echo -e "\nBacking up distribution lists"
su - zimbra -c su - zimbra -c 'zmprov gadl > /backups/zmove/dlist.txt'
cat /backups/zmove/dlist.txt


echo -e "\nBacking up distribution list members"
su - zimbra -c "mkdir /backups/zmove/dlist_members"
su - zimbra -c 'for i in `cat /backups/zmove/dlist.txt`; do zmprov gdlm $i > /backups/zmove/dlist_members/$i.txt ;echo "$i"; done'



echo -e "\nBacking up aliases"
su - zimbra -c "mkdir /backups/zmove/alias"
su - zimbra -c 'for i in `cat /backups/zmove/emails.txt`; do zmprov ga  $i | grep zimbraMailAlias |cut -d" " -f2 > /backups/zmove/alias/$i.txt ;echo $i ;done'

echo -e "\nremoving accounts that do not have aliases"
find /backups/zmove/alias/ -type f -empty | xargs -n1 rm -v
